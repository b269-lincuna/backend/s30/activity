
// Create documents to use for the activity
db.fruits.insertMany([
    { name : "Apple", color : "Red",    stock : 20,
      price: 40,      supplier_id : 1,  onSale : true,
      origin: [ "Philippines", "US" ]},
    { name : "Banana", color : "Yellow",stock : 15,
      price: 20,       supplier_id : 2, onSale : true,
      origin: [ "Philippines", "Ecuador" ]},
    { name : "Kiwi", color : "Green",   stock : 25,
      price: 50,     supplier_id : 1,   onSale : true,
      origin: [ "US", "China" ]},
    { name : "Mango", color : "Yellow", stock : 10,
      price: 120, supplier_id : 2,      onSale : false,
      origin: [ "Philippines", "India" ]}
]);

// ************************************************
// **************** ACTIVITY START ****************
// ************************************************

    // Use the count operator to count the total number of fruits on sale.
        db.fruits.aggregate([
            {$match: { onSale: true }},
            {$group: { _id:"$onSale", fruits_on_sale: {$count:{}}}},
            {$project:{_id:0}}
        ]);
//----------------------------
    // Use the count operator to count the total number of fruits with 
    // stock more than or equal to  20.
        db.fruits.aggregate([
            {$match: { stock: { $gte: 20 } }},
            {$group: { _id:null, enoughStock: {$count:{}}}},
            {$project:{_id:0}}
        ]);
//----------------------------
    // Use the average operator to get the average price of fruits onSale 
    // per supplier.
        db.fruits.aggregate([
            {$match: { onSale: true }},
            {$group: { _id:"$supplier_id", avgPrice:{$avg: "$price" }}}
        ]);
//----------------------------
    // Use the MAX operator to get the highest price of a fruit per 
    // supplier.
        db.fruits.aggregate([
            {$group: { _id:"$supplier_id", maxPrice:{$max: "$price" }}}
        ]);
//----------------------------
    //Use the MIN operator to get the lowest price of a fruit per supplier.
        db.fruits.aggregate([
            {$group: { _id:"$supplier_id", minPrice:{$min: "$price" }}}
        ]);

// *****************************************************
// ******************  REFERENCES  *********************
// *****************************************************
/*  
  MongoDB aggregation
  https://docs.mongodb.com/manual/aggregation/

  MongoDB $match
  https://docs.mongodb.com/manual/reference/operator/aggregation/match/

  MongoDB $group
  https://docs.mongodb.com/manual/reference/operator/aggregation/group/

  MongoDB $sum
  https://docs.mongodb.com/manual/reference/operator/aggregation/sum/

  MongoDB Data Models
  https://docs.mongodb.com/manual/core/data-model-design/

  MongoDB $project
  https://docs.mongodb.com/manual/reference/operator/aggregation/project/

  MongoDB $sort  
  https://docs.mongodb.com/manual/reference/operator/aggregation/sort/

  MongoDB $unwind    
  https://docs.mongodb.com/manual/reference/operator/aggregation/unwind/

  MongoDB $count
  https://docs.mongodb.com/manual/reference/operator/aggregation/count/

  MongoDB $avg
  https://docs.mongodb.com/manual/reference/operator/aggregation/avg/

  MongoDB $min
  https://docs.mongodb.com/manual/reference/operator/aggregation/min/

  MongoDB $max
  https://docs.mongodb.com/manual/reference/operator/aggregation/max/
*/